  // import anime from 'animejs/lib/anime.es.js';
  const staggerVisualizerEl = document.querySelector('.stagger-visualizer');
  const main_array = [];
  let curdivoffset = [];
  let divOfSetDiff = [];
  let speed = 500;
  let new_divs = [];
  let n = 10;



  function createArrayList(n) {
      for (let i = 0; i < n; i++) {
          let rndmarr = Math.floor(Math.random() * 100) + 1;
          main_array.push(rndmarr);
          new_divs.push(i);

      }
  }

  createArrayList(n);
  for (let i = 0; i < n; i++) {
      let div = document.createElement("div");
      // div.style.backgroundColor = "blue";
      div.style.width = 60 + 'px';
      div.style.height = (main_array[i] * 5) + 'px';

      // this will hide the text inside the bars after size gtreater thet 40
      div.innerHTML = "<span>" + main_array[i] + "</span>";

      staggerVisualizerEl.appendChild(div);
      //it added the div in the array_container class

  }

  let divs = document.querySelectorAll('.stagger-visualizer div')
  for (i = 0; i < n; i++)
      curdivoffset[i] = divs[i].offsetLeft, divOfSetDiff[i] = 0;




  function swapAnimation(i1, i2) {
      console.log(curdivoffset);

      var divDiff = curdivoffset[i2] - curdivoffset[i1];
      divOfSetDiff[i1] = divOfSetDiff[i1] + divDiff;
      divOfSetDiff[i2] = divOfSetDiff[i2] + (-divDiff);
      let tempvalue = curdivoffset[i1];
      curdivoffset[i1] = curdivoffset[i2];
      curdivoffset[i2] = tempvalue;





      // console.log("div1: "+div1+"\t div2:" + div2);
      console.log(curdivoffset);
      const staggersAnimation = anime.timeline({

              easing: 'easeInOutSine',
              delay: anime.stagger(50),
              loop: false,
              autoplay: true,
              targets: divs[i1],
              duration: speed / 2,
          })

          .add({
              translateY: -40,
          })
          .add({
              translateX: divOfSetDiff[i1]
          })
          .add({
              translateY: 0.1
          })


      const sV2 = anime.timeline({
              easing: 'easeInOutSine',
              delay: anime.stagger(50),
              loop: false,
              autoplay: true,
              targets: divs[i2],
              duration: speed / 2
          })
          .add({
              translateY: -40,
          })
          .add({
              translateX: divOfSetDiff[i2],

          })
          .add({
              translateY: 0
          })
      // divColor(1,2,false);
      console.log("heelo");

      // divColor(i1,i2,true,"green");
      // setTimeout(function(){  divColor(i1,i2,false,"");  }, speed);
  }











  function divColor(idx1, idx2, set, clr) {
      if (set) {
          divs[idx1].style.backgroundColor = clr;
          divs[idx2].style.backgroundColor = clr;
      } else {
          divs[idx1].style.backgroundColor = "white";
          divs[idx2].style.backgroundColor = "white";
      }

  }











  array = main_array;



  function swap(array, first_Index, second_Index) {

      // array value swaping
      console.log(array[first_Index], array[second_Index])

      var temp = array[first_Index];
      array[first_Index] = array[second_Index];
      array[second_Index] = temp;
      swapAnimation(new_divs[first_Index], new_divs[second_Index]);
      // new divs index swaping
      temp = new_divs[first_Index];
      new_divs[first_Index] = new_divs[second_Index];
      new_divs[second_Index] = temp;


  }



  function timeout(ms) {
      return new Promise(resolve => setTimeout(resolve, ms))
  }

  // async function slowedCode() {
  //   console.log("Before Delay")
  //   await this.timeout(Math.random() * 2000 + 500) // Wait random amount of time between [0.5, 2.5] seconds
  //   console.log("After Delay")
  // }



  async function bubble_Sort(array) {

      var len = array.length,
          i, j;


      for (i = 0; i < len; i++) {
          for (j = 0; j < len - i; j++) {
              // if(j<len-1){
              //  divColor(j,j+1,true,"blue");
              //  await this.timeout(speed/9);
              //  divColor(j,j+1,false,"");
              // }
              // await this.timeout(200);
              // divColor(j,j++,false,"blue")
              //  setTimeout(function(){  divColor(j,j++,false,"");  }, 500);
              if (array[j] > array[j + 1]) {
                  divColor(j, j + 1, true, "blue");
                  swap(array, j, j + 1);
                  await this.timeout(0);
                  divColor(j, j + 1, false, "");
              }

              await this.timeout(speed * 2);
          }

          await this.timeout(20);

      }


  }

  setTimeout(function () {
      bubble_Sort(array);
  }, 2000);